
const path = require('path');

const bcrypt = require('bcryptjs');


const Auth = require("flyauth");

module.exports = class {
  constructor(cmbird, user_auth_cfg) {
    var auth = this.auth = cmbird.auth;

    let pages = cmbird.pages, table = cmbird.auth.table;
    this.pages = pages;
    this.table = table;

    var dist_path = path.resolve(__dirname, 'dist');
    
    const path_prefix = "/content-manager";

    cmbird.pages.serve_dirs('/administravimas', dist_path, {
      auth: cmbird.admin.auth,
      name: "administravimas",
      globals_path: path.resolve(__dirname, "globals")
    });

    let page = cmbird.cms.serve_extra_page("Users", path.resolve(__dirname, 'dist'));

    let app = cmbird.app;
    app.post(path_prefix+"/users", cmbird.admin.auth.orize_gen(["content_management"]), async function(req, res) {
      try {
        let data = JSON.parse(req.body.data);
        switch (data.command) {
          case "all":
            const columns_to_select = [
              "id", "email", "cfg"
            ];
            if (user_auth_cfg.save_ip_addrs) {
              columns_to_select.push("ip_addrs");
              columns_to_select.push("fingerprints");
            }
            if (user_auth_cfg.custom_columns["username"]) columns_to_select.push("username");
            const all = await table.select(columns_to_select);
            res.json(all);
            break;
          case "invite":
            data.invitation = true;
            await cmbird.auth.signup_email(data);
            res.send("OK");
            break;
          case "add":
            const existing = await table.select(
              ["email"],
              "email = $1",
              [data.email]
            );

            if (existing.length > 0) {
              res.send("EMAIL ALREADY IN USE!");
            } else {
              var salt = bcrypt.genSaltSync(10);
              data.password = bcrypt.hashSync(data.password, salt);
            //  console.log(data);
              if (data.cfg === "") {
                data.cfg = "{}";
              }
              await table.insert(data);
              res.send("success");
            }
            break;
          case "rm":
          console.log("REMOVE", data.email);
            await table.delete(
              "email = $1",
              [data.email]
            );
            res.send("success");
            break;
          case "edit":
            try {
              data = data.data;
              await table.update(
                { super: data.super, cfg: data.cfg },
                "email = $1",
                [data.email]
              );
              res.send("success");
            } catch (e) {
              res.send(e.message);
            }
            break;
            res.send(result);
            break;
          default:
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    auth.builtin_pages(cmbird.pages, {
      globals_path: cmbird.globals_path
    });
  }

  static async init(cmbird, cfg) {
    try {
      console.log("+", "Users");

      let user_auth_cfg = cmbird.get_module_cfg(cfg.name);

      cmbird.auth = await Auth.init(cmbird.app, cmbird.mail_session, cmbird.aura, user_auth_cfg || { lang: user_auth_cfg.lang });
//      cmbird.set_auth(cmbird.auth);

      return new module.exports(cmbird, user_auth_cfg);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
