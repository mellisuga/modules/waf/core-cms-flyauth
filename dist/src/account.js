

const XHR = require('core/utils/xhr_async.js');

const CodeMirror = require("core/codemirror.ui/index.js");

const html = require('./account.html');

module.exports = class {
  constructor(cfg) {
    console.log("ACC", cfg);
    this.element = document.createElement("tr");
    this.element.classList.add("admin_accounts_ui_item");
    this.element.innerHTML = html;

    this.email = cfg.email;

    var name = this.element.querySelector(".admin_accounts_ui_item_name")
    name.innerHTML = cfg.id+"<br>"+cfg.email;


    let inputs = this.element.getElementsByTagName("input");
    if (!inputs) inputs = [];

    let this_class = this;

    let totp_td = this.element.querySelector(".totp");

    let usrn_td = document.createElement("td");
    if (cfg.username) {
      usrn_td.innerHTML = cfg.username;
    }
    this.element.insertBefore(usrn_td, totp_td);

    if (cfg.totp_enabled) {
      totp_td.innerHTML = "ENABLED";
    } else {
      let totp_btn = document.createElement("button");
      totp_btn.innerHTML = "Generate";
      totp_btn.disabled = "disabled";
      totp_btn.addEventListener("click", async function(e) {
        try {
          if (confirm("Are you sure you want to secure this account with TOTP?")) {
            let result = await XHR.post("/content-manager/users", {
              command: "generate_totp",
              data: {
                uid: cfg.email
              }
            }, 'access_token');

            console.log(result);
          }
        } catch (ex) {
          console.error(ex.stack);
        }
      });
      totp_td.appendChild(totp_btn);
    }

    var json_edit = this.element.querySelector(".json_edit")

    let ip_addrs_td = document.createElement("td");
    ip_addrs_td.classList.add("ip_addrs_td");
    if (cfg.ip_addrs) {
      ip_addrs_td.innerHTML = "<div><p>"+cfg.ip_addrs.join("</p><p>")+"</p></div>";
    }
    this.element.insertBefore(ip_addrs_td, json_edit);

    let fps_td = document.createElement("td");
    fps_td.classList.add("fps_td");
    if (cfg.fingerprints) {
      fps_td.innerHTML = "<div><p>"+cfg.fingerprints.join("</p><p>")+"</p></div>";
    }
    this.element.insertBefore(fps_td, json_edit);

    this.cfg_string = JSON.stringify(JSON.parse(cfg.cfg || '{}'), null, 2)

    this.html_editor = new CodeMirror(this.cfg_string, 'js', true);
    json_edit.appendChild(this.html_editor.element);
    this.html_editor.cm.setSize(744, 200);

    setTimeout(function() {
      this_class.html_editor.cm.refresh();
    },1);

    let edit_btn = this.element.querySelector(".admin_accounts_ui_item_edit");
    let rm_btn = this.element.querySelector(".admin_accounts_ui_item_remove");



    rm_btn.addEventListener("click", async function(e) {
      const response = await XHR.post("/content-manager/users", {
        command: "rm",
        data: {
          email: this_class.email
        }
      }, 'access_token');
      if (response === "success") {
        this_class.destroy();
      } else {
        console.log(response);
      }
    });
    let edit_save = this.element.querySelector(".admin_accounts_ui_item_save");
    edit_btn.addEventListener("click", function(e) {
      edit_btn.style.display = "none";
      rm_btn.style.display = "none";
      edit_save.style.display = "inline-block";

      for (var i = 0; i < inputs.length; i++) {
        var input = inputs[i];
        input.disabled = false;
      }

      json_edit.removeChild(this_class.html_editor.element);
      json_edit.style.backgroundColor = "#222";
      this_class.html_editor = new CodeMirror(this_class.cfg_string, 'js', false);
      json_edit.appendChild(this_class.html_editor.element);
      this_class.html_editor.cm.setSize(744, 200);
      this_class.html_editor.cm.refresh();
    });

    edit_save.addEventListener("click", async function(e) {
      var cfg_json = this_class.html_editor.cm.getValue();
      this_class.cfg_string = cfg_json;

      console.log("JSON", cfg_json);

      const response = await XHR.post("/content-manager/users", {
        command: "edit",
        data: {
          email: this_class.email,
          cfg: cfg_json
        }
      }, 'access_token');
      if (response === "success") {
        edit_btn.style.display = "inline-block";
        rm_btn.style.display = "inline-block";
        edit_save.style.display = "none";

        for (var i = 0; i < inputs.length; i++) {
          var input = inputs[i];
          input.disabled = true;
        }

        json_edit.removeChild(this_class.html_editor.element);
        json_edit.style.backgroundColor = "transparent";
        this_class.html_editor = new CodeMirror(cfg_json, 'js', true);
        json_edit.appendChild(this_class.html_editor.element);
        this_class.html_editor.cm.setSize(500, 100);
        this_class.html_editor.cm.refresh();
      } else {
        alert(response);
      }
    });
  }

  destroy() {
    this.element.parentNode.removeChild(this.element);
  }
}
