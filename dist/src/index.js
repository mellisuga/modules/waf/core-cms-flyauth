

const XHR = require("core/utils/xhr_async");
const CodeMirror = require("core/codemirror.ui/index.js");
const Account = require('./account.js');


let accounts_table = document.getElementById('accounts_table');

let invite_user = {
  block: document.body.querySelector('.send_invitation_block')
}
invite_user.email = invite_user.block.querySelector("input[type=text]");
invite_user.submit = invite_user.block.querySelector("input[type=submit]");

let add_user = {
  block: document.body.querySelector('.add_user_block')
}
add_user.email = add_user.block.querySelector("input[type=text]");
add_user.pwd1 = add_user.block.querySelector("input[type=password]:nth-child(3)");
add_user.pwd2 = add_user.block.querySelector("input[type=password]:nth-child(4)");
add_user.submit = add_user.block.querySelector("input[type=submit]");

(async function() {
  try {
//    await require("globals/header.js").construct();

    invite_user.submit.addEventListener("click", async function(e) {
      try {
        let email_value = invite_user.email.value;
        if (email_value.length > 0) {
          console.log("ADD USER", email_value);

          let add_res = await XHR.post("/content-manager/users", {
            command: "invite",
            email: email_value
          }, "access_token");
          console.log("RESULT", add_res);
          window.location.reload(true);
        } else {
          alert("NO EMAIL ADRESS");
        }
      } catch (ex) {
        console.error(ex.stack);
      }
    });

    add_user.submit.addEventListener("click", async function(e) {
      try {
        let email_value = add_user.email.value;
        let pwd1_value = add_user.pwd1.value;
        let pwd2_value = add_user.pwd2.value;
        if (email_value.length > 0 && pwd1_value.length > 0 && pwd1_value == pwd2_value) {
          console.log("ADD USER", email_value);

          let add_res = await XHR.post("/content-manager/users", {
            command: "add",
            email: email_value,
            password: pwd1_value
          }, "access_token");
          console.log("RESULT", add_res);
          window.location.reload(true);
        } else {
          if (!email_value.length > 0) alert("NO EMAIL ADRESS");
          if (!pwd1_value.length > 0) alert("NO PASSWORD");
          if (pwd1_value != pwd2_value) alert("PASSWORDS DON'T MATCH");

        }
      } catch (ex) {
        console.error(ex.stack);
      }
    });

    const accounts = await XHR.post("/content-manager/users", {
      command: 'all',
    }, 'access_token');

    for (var a = 0; a < accounts.length; a++) {

      accounts[a].cfg = JSON.stringify(accounts[a].cfg);
      var account = new Account(accounts[a]);
      accounts_table.appendChild(account.element);
    //  accounts_array.push(account);
      
/*
      let account = accounts[a];
      let tr = document.createElement('tr');

      let email_td = document.createElement('td');
      email_td.innerHTML = account.email;
      tr.appendChild(email_td);

      let cfg_td = document.createElement('td');

      let json_editor = new CodeMirror(JSON.stringify(account.cfg, null, 2), 'js', true);
      cfg_td.appendChild(json_editor.element);
      tr.appendChild(cfg_td);
      json_editor.cm.setSize(800, 200);
      setTimeout(function() {
        json_editor.cm.refresh();
      },1);


      let options_td = document.createElement("td");
      tr.appendChild(options_td);

      accounts_table.appendChild(tr);
      */
    }
    hide_loading_overlay();
  } catch (e) {
    console.error(e.stack);
  }
})();
